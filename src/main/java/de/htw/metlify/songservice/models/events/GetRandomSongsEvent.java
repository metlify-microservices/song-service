package de.htw.metlify.songservice.models.events;

public class GetRandomSongsEvent {

    private String genre;

    public GetRandomSongsEvent(String genre) {
        this.genre = genre;
    }

    public GetRandomSongsEvent() {
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
