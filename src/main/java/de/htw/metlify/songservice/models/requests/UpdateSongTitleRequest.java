package de.htw.metlify.songservice.models.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateSongTitleRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @NotEmpty
    @NotBlank
    private String newTitle;

    public UpdateSongTitleRequest(int id, String newTitle) {
        this.id = id;
        this.newTitle = newTitle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewTitle() {
        return newTitle;
    }

    public void setNewTitle(String newTitle) {
        this.newTitle = newTitle;
    }
}
