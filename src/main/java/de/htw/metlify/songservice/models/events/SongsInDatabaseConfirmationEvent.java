package de.htw.metlify.songservice.models.events;

import java.io.Serializable;

public class SongsInDatabaseConfirmationEvent implements Serializable {

    private boolean areSongsInDatabase;

    public SongsInDatabaseConfirmationEvent(boolean areSongsInDatabase) {
        this.areSongsInDatabase = areSongsInDatabase;
    }

    public SongsInDatabaseConfirmationEvent() {
    }

    public boolean isAreSongsInDatabase() {
        return areSongsInDatabase;
    }

    public void setAreSongsInDatabase(boolean areSongsInDatabase) {
        this.areSongsInDatabase = areSongsInDatabase;
    }
}
