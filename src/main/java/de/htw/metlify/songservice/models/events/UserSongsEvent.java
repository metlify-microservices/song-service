package de.htw.metlify.songservice.models.events;

import java.util.Set;

public class UserSongsEvent {
    private Set<Integer> songIds;

    public UserSongsEvent(Set<Integer> songIds) {
        this.songIds = songIds;
    }

    public UserSongsEvent() {
    }

    public Set<Integer> getSongIds() {
        return songIds;
    }

    public void setSongIds(Set<Integer> songIds) {
        this.songIds = songIds;
    }
}
