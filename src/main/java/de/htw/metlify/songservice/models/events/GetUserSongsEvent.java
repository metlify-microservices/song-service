package de.htw.metlify.songservice.models.events;

public class GetUserSongsEvent {
    private int userId;

    public GetUserSongsEvent(int userId) {
        this.userId = userId;
    }

    public GetUserSongsEvent() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
