package de.htw.metlify.songservice.models.events;

import java.util.List;

public class RandomSongsEvent {
    private List<Integer> randomSongIds;

    public RandomSongsEvent(List<Integer> randomSongIds) {
        this.randomSongIds = randomSongIds;
    }

    public RandomSongsEvent() {
    }

    public List<Integer> getRandomSongIds() {
        return randomSongIds;
    }

    public void setRandomSongIds(List<Integer> randomSongIds) {
        this.randomSongIds = randomSongIds;
    }
}
