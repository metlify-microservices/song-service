package de.htw.metlify.songservice.models.requests;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateSongIsExplicitRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    private boolean isExplicit;

    public UpdateSongIsExplicitRequest(int id, boolean isExplicit) {
        this.id = id;
        this.isExplicit = isExplicit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isExplicit() {
        return isExplicit;
    }

    public void setExplicit(boolean explicit) {
        isExplicit = explicit;
    }
}
