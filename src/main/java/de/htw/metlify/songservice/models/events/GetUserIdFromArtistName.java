package de.htw.metlify.songservice.models.events;

public class GetUserIdFromArtistName {

    private String artistName;

    public GetUserIdFromArtistName(String artistName) {
        this.artistName = artistName;
    }

    public GetUserIdFromArtistName() {
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }
}
