package de.htw.metlify.songservice.models.requests;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateSongLengthRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @Positive
    private int newLength;

    public UpdateSongLengthRequest(int id, int length) {
        this.id = id;
        this.newLength = length;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNewLength() {
        return newLength;
    }

    public void setNewLength(int newLength) {
        this.newLength = newLength;
    }
}
