package de.htw.metlify.songservice.models.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateSongAlbumRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @NotEmpty
    @NotBlank
    private String newAlbum;

    public UpdateSongAlbumRequest(int id, String album) {
        this.id = id;
        this.newAlbum = album;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewAlbum() {
        return newAlbum;
    }

    public void setNewAlbum(String newAlbum) {
        this.newAlbum = newAlbum;
    }
}
