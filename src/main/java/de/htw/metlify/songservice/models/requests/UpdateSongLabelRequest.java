package de.htw.metlify.songservice.models.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateSongLabelRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @NotEmpty
    @NotBlank
    private String newLabel;

    public UpdateSongLabelRequest(int id, String newLabel) {
        this.id = id;
        this.newLabel = newLabel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewLabel() {
        return newLabel;
    }

    public void setNewLabel(String newLabel) {
        this.newLabel = newLabel;
    }
}
