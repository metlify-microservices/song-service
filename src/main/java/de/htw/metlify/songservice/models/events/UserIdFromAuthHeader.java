package de.htw.metlify.songservice.models.events;

public class UserIdFromAuthHeader {
    private int userId;

    public UserIdFromAuthHeader(int userId) {
        this.userId = userId;
    }

    public UserIdFromAuthHeader() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
