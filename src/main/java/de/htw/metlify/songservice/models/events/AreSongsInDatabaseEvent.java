package de.htw.metlify.songservice.models.events;

import java.io.Serializable;
import java.util.Set;

public class AreSongsInDatabaseEvent implements Serializable {
    private Set<Integer> songIds;

    public AreSongsInDatabaseEvent(Set<Integer> songIds) {
        this.songIds = songIds;
    }
    public AreSongsInDatabaseEvent(){}

    public Set<Integer> getSongIds() {
        return songIds;
    }

    public void setSongIds(Set<Integer> songIds) {
        this.songIds = songIds;
    }
}
