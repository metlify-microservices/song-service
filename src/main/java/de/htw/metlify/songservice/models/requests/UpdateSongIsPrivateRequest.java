package de.htw.metlify.songservice.models.requests;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateSongIsPrivateRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    private boolean isPrivate;

    public UpdateSongIsPrivateRequest(int id, boolean isPrivate) {
        this.id = id;
        this.isPrivate = isPrivate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }
}
