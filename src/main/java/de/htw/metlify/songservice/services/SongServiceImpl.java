package de.htw.metlify.songservice.services;


import de.htw.metlify.songservice.models.Song;
import de.htw.metlify.songservice.models.requests.*;
import de.htw.metlify.songservice.repositories.SongRepository;
import de.htw.metlify.songservice.specification.SongSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SongServiceImpl implements SongService {

    private final SongRepository songRepository;
    private final boolean IS_NOT_PRIVATE = false;

    @Autowired
    public SongServiceImpl(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @Override
    public boolean isSongInDatabase(Integer id) {
        return false;
    }

    @Override
    public boolean areSongsInDatabase(Set<Integer> songIds) {
        for (int id : songIds) {
            if (!songRepository.existsById(id)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Set<Song> getAllSongsWithMultipleIds(Set<Integer> songIds) {
        List<Song> songsList = songRepository.findAllById(songIds);
        return new HashSet<>(songsList);
    }

    @Override
    public Song findSongById(Integer id) {
        Optional<Song> songOptional = songRepository.findById(id);
        if (songOptional.isEmpty()) {
            return null;
        } else {
            return songOptional.get();
        }
    }

    @Override
    public List<Song> getSongsByParameter(String parameter, String value) {
        SongSpecification songSpecification = new SongSpecification(parameter, value);
        List<Song> songs = songRepository.findAll(songSpecification);
        List<Song> publicSongs = filterOutPrivateSongs(songs);

        return publicSongs;
    }

    @Override
    public List<Song> getRandomSongsByGenre(String genre) {
        return songRepository.findByGenreRandom(genre);
    }

    private List<Song> filterOutPrivateSongs(List<Song> songs) {
        return songs.stream().filter(song -> !song.isPrivate()).collect(Collectors.toList());
    }

    @Override
    public List<Song> findSongsByArtist(int artist) {
        return songRepository.findByUserIdAndIsPrivate(artist, IS_NOT_PRIVATE);
    }

    @Override
    public List<Song> findAllSongsByArtist(int artist) {
        return songRepository.findByUserId(artist);
    }

    @Override
    public List<Song> findSongsByReleased(Integer released) {
        return songRepository.findByReleasedAndIsPrivate(released, IS_NOT_PRIVATE);
    }

    @Override
    public List<Song> findSongsByIsExplicit(boolean isExplicit) {
        return songRepository.findByIsExplicitAndIsPrivate(isExplicit, IS_NOT_PRIVATE);
    }

    @Override
    public Song createSong(Song song) {
        return songRepository.save(song);
    }

    @Override
    public Song updateTitle(UpdateSongTitleRequest request) {
        String newTitle = request.getNewTitle();
        Song songFromDB = songRepository.findById(request.getId());
        songFromDB.setTitle(newTitle);

        return songRepository.save(songFromDB);
    }

    @Override
    public Song updateLabel(UpdateSongLabelRequest request) {
        String newLabel = request.getNewLabel();
        Song songFromDB = songRepository.findById(request.getId());
        songFromDB.setLabel(newLabel);

        return songRepository.save(songFromDB);
    }

    @Override
    public Song updateIsExplicit(UpdateSongIsExplicitRequest request) {
        boolean isExplicit = request.isExplicit();
        Song songFromDB = songRepository.findById(request.getId());
        songFromDB.setExplicit(isExplicit);

        return songRepository.save(songFromDB);
    }

    @Override
    public Song updateLength(UpdateSongLengthRequest request) {
        int newLength = request.getNewLength();
        Song songFromDB = songRepository.findById(request.getId());
        songFromDB.setLength(newLength);

        return songRepository.save(songFromDB);
    }

    @Override
    public Song updateAlbum(UpdateSongAlbumRequest request) {
        String newAlbum = request.getNewAlbum();
        Song songFromDB = songRepository.findById(request.getId());
        songFromDB.setAlbum(newAlbum);

        return songRepository.save(songFromDB);
    }

    @Override
    public Song updateIsPrivate(UpdateSongIsPrivateRequest request) {
        boolean isPrivate = request.isPrivate();
        Song songFromDB = songRepository.findById(request.getId());
        songFromDB.setPrivate(isPrivate);

        return songRepository.save(songFromDB);
    }

    @Override
    public Song updateGenre(UpdateSongGenreRequest request) {
        String newGenre = request.getNewGenre();
        Song songFromDB = songRepository.findById(request.getId());
        songFromDB.setGenre(newGenre);

        return songRepository.save(songFromDB);
    }

    @Override
    public void deleteSong(Integer id) {
        songRepository.deleteById(id);
    }

    @Override
    public void deleteSongsByUser(int userId) {
        songRepository.deleteByUserId(userId);
    }
}
