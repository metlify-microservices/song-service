package de.htw.metlify.songservice.services;

public interface MessagingService {
    int getUserIdFromAuthHeader(String authHeader);

    int getUserIdByUsername(String artistName);

    void deleteSongInAllServices(Integer id);
}
