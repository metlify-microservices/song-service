package de.htw.metlify.songservice.services;

import de.htw.metlify.songservice.models.events.*;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class QueueProducer implements MessagingService {

    private static final String ROUTING_KEY_AUTH_HEADER = "userId.header.song";
    private static final String ROUTING_KEY_ARTIST_NAME = "userId.artistName";
    private static final String ROUTING_KEY_DELETE_SONG = "delete.song";
    private final RabbitTemplate template;
    private final DirectExchange directExchange;

    public QueueProducer(RabbitTemplate template, @Qualifier("songsDirectExchange") DirectExchange directExchange) {
        this.template = template;
        this.directExchange = directExchange;
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public int getUserIdFromAuthHeader(String authHeader) {
        GetUserIdFromAuthHeaderEvent event = new GetUserIdFromAuthHeaderEvent(authHeader);

        UserIdFromAuthHeader response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_AUTH_HEADER,
                event,
                new ParameterizedTypeReference<>() {
                });

        if (response != null) {
            return response.getUserId();
        } else {
            return 0;
        }
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public int getUserIdByUsername(String artistName) {
        GetUserIdFromArtistName event = new GetUserIdFromArtistName(artistName);

        UserIdFromArtistName response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_ARTIST_NAME,
                event,
                new ParameterizedTypeReference<>() {
                });


        if (response != null) {
            return response.getUserId();
        } else {
            return 0;
        }
    }

    @Override
    public void deleteSongInAllServices(Integer id) {
        DeleteSongEvent event = new DeleteSongEvent(id);

        template.convertAndSend(directExchange.getName(),ROUTING_KEY_DELETE_SONG, event);
    }
}
