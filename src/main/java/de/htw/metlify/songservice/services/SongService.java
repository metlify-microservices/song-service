package de.htw.metlify.songservice.services;



import de.htw.metlify.songservice.models.Song;
import de.htw.metlify.songservice.models.requests.*;

import java.util.List;
import java.util.Set;

public interface SongService {
    boolean isSongInDatabase(Integer id);
    boolean areSongsInDatabase(Set<Integer> songIds);
    Set<Song> getAllSongsWithMultipleIds(Set<Integer> songIds);
    Song findSongById(Integer id);
    List<Song> findSongsByArtist(int artist);

    List<Song> findAllSongsByArtist(int artist);

    List<Song> findSongsByReleased(Integer released);
    List<Song> findSongsByIsExplicit(boolean isExplicit);
    Song createSong(Song song);
    Song updateTitle(UpdateSongTitleRequest request);
    Song updateLabel(UpdateSongLabelRequest request);
    Song updateIsExplicit(UpdateSongIsExplicitRequest request);
    Song updateLength(UpdateSongLengthRequest request);
    Song updateAlbum(UpdateSongAlbumRequest request);
    Song updateIsPrivate(UpdateSongIsPrivateRequest request);
    Song updateGenre(UpdateSongGenreRequest request);
    List<Song> getSongsByParameter(String parameter, String value);
    List<Song> getRandomSongsByGenre(String genre);
    void deleteSong(Integer id);
    void deleteSongsByUser(int id);
}
