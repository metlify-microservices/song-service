package de.htw.metlify.songservice.specification;

import de.htw.metlify.songservice.models.Song;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class SongSpecification implements Specification<Song> {
    private String variableColumnName;

    private String valueToSearchFor;

    public SongSpecification(String variableColumnName, String valueToSearchFor) {
        this.variableColumnName = variableColumnName;
        this.valueToSearchFor = valueToSearchFor;
    }

    @Override
    public Predicate toPredicate(Root<Song> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
       return criteriaBuilder.and(criteriaBuilder.equal(root.<String>get(this.variableColumnName), this.valueToSearchFor));
    }
}
