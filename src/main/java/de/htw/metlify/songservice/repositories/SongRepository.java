package de.htw.metlify.songservice.repositories;

import de.htw.metlify.songservice.models.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SongRepository extends JpaRepository<Song, Integer>, JpaSpecificationExecutor<Song> {
    Song findById(int id);
    List<Song> findAllById(Iterable<Integer> ids);
    List<Song> findByTitleAndIsPrivate(String title, boolean isPrivate);
    List<Song> findByUserIdAndIsPrivate(int artist, boolean isPrivate);
    List<Song> findByReleasedAndIsPrivate(Integer released, boolean isPrivate);
    List<Song> findByGenreAndIsPrivate(String genre, boolean isPrivate);
    List<Song> findByLabelAndIsPrivate(String label, boolean isPrivate);
    List<Song> findByIsExplicitAndIsPrivate(boolean isExplicit, boolean isPrivate);
    List<Song> findByAlbumAndIsPrivate(String album, boolean isPrivate);
    @Query(value="SELECT * FROM song WHERE genre = :#{#genre} ORDER BY RAND() LIMIT 5",nativeQuery = true)
    List<Song> findByGenreRandom(@Param("genre") String genre);
    @Transactional
    void deleteByUserId(int userId);

    List<Song> findByUserId(int artist);
}
