package de.htw.metlify.songservice.queues;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfig {

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("confirmation.songs");
    }

    @Bean DirectExchange songsDirectExchange() {
        return new DirectExchange("exchange.song");
    }

    @Bean DirectExchange playlistsDirectExchange() {
        return new DirectExchange("exchange.playlist");
    }

    @Bean
    public Queue response() {
        return new Queue("response");
    }

    @Bean
    public Queue deleteUser() {
        return new Queue("deleteUserSong");
    }
    @Bean
    public Queue getUserSongsResponse() {
        return new Queue("getUserSongsResponse");
    }

    @Bean
    public Queue getRandomSongsResponse() {
        return new Queue("randomSongsResponse");
    }


    @Bean
    public Binding songsConfirmationBinding(DirectExchange directExchange,
                                            @Qualifier("response") Queue responseQueue) {
        return BindingBuilder.bind(responseQueue)
                .to(directExchange)
                .with("old.songs");
    }

    @Bean
    public Binding songsConfirmationPlaylistBinding(@Qualifier("playlistsDirectExchange") DirectExchange directExchange,
                                            @Qualifier("response") Queue responseQueue) {
        return BindingBuilder.bind(responseQueue)
                .to(directExchange)
                .with("old.songs");
    }

    @Bean
    public Binding songsRandomPlaylistBinding(@Qualifier("playlistsDirectExchange") DirectExchange directExchange,
                                                    @Qualifier("getRandomSongsResponse") Queue responseQueue) {
        return BindingBuilder.bind(responseQueue)
                .to(directExchange)
                .with("random.songs");
    }

    @Bean
    public Binding deleteUserBinding(DirectExchange directExchange, @Qualifier("deleteUser") Queue deleteUserQueue)  {
        return BindingBuilder.bind(deleteUserQueue)
                .to(directExchange)
                .with("delete.user");
    }

    @Bean
    public Binding getUserSongsBinding(DirectExchange directExchange, @Qualifier("getUserSongsResponse") Queue getUserSongsResponse)  {
        return BindingBuilder.bind(getUserSongsResponse)
                .to(directExchange)
                .with("user.songs");
    }

    @Bean
    public MessageConverter jackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
