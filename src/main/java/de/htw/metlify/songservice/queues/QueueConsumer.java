package de.htw.metlify.songservice.queues;


import de.htw.metlify.songservice.models.Song;
import de.htw.metlify.songservice.models.events.*;
import de.htw.metlify.songservice.services.SongService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class QueueConsumer {

    @Autowired
    private SongService songService;

    @RabbitListener(queues = "response", concurrency = "3")
    public SongsInDatabaseConfirmationEvent receive(AreSongsInDatabaseEvent event) {
        Set<Integer> songIds = event.getSongIds();
        boolean result = songService.areSongsInDatabase(songIds);
        SongsInDatabaseConfirmationEvent answer = new SongsInDatabaseConfirmationEvent(result);

        return answer;
    }


    @RabbitListener(queues = "deleteUserSong", concurrency = "3")
    public void receiveDeleteUser(DeleteUsersEvent event) {
        int userId = event.getUserId();
        songService.deleteSongsByUser(userId);
    }

    @RabbitListener(queues = "getUserSongsResponse", concurrency = "3")
    public UserSongsEvent receiveUserSongsResponse(GetUserSongsEvent event) {
        int userId = event.getUserId();
        List<Song> songList = songService.findAllSongsByArtist(userId);

        Set<Integer> songIds = new HashSet<>();
        for (Song song : songList) {
            songIds.add(song.getId());
        }
        UserSongsEvent answer = new UserSongsEvent(songIds);

        return answer;
    }

    @RabbitListener(queues = "randomSongsResponse", concurrency = "3")
    public RandomSongsEvent receiveRandomSongsResponse(GetRandomSongsEvent event) {
        String genre = event.getGenre();
        List<Song> songs = songService.getRandomSongsByGenre(genre);

        List<Integer> songIds = new ArrayList<>();
        for (Song song: songs) {
            songIds.add(song.getId());
        }
        RandomSongsEvent answer = new RandomSongsEvent(songIds);

        return answer;
    }
}
