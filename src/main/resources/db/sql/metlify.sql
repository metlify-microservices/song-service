
INSERT INTO metlify.song (id, title, user_id, released, label, genre, is_explicit, length, album, is_private) VALUES (1, 'Intro', 2, 2019, 'Selfmade', 'Progressive Metal', false, 165, 'Epic Shadows', false);
INSERT INTO metlify.song (id, title, user_id, released, label, genre, is_explicit, length, album, is_private) VALUES (2, 'Ready to Kill', 2, 2019, 'Selfmade', 'Progressive Metal', true, 354, 'Epic Shadows', false);
INSERT INTO metlify.song (id, title, user_id, released, label, genre, is_explicit, length, album, is_private) VALUES (3, 'TestSong1', 1, 2018, 'peter123', 'Hard Rock', false, 300, 'Test', true);
